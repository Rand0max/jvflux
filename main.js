
function getCurrentPageType(url) {
    let topicMessagesRegex = /^\/forums\/(42|1)-[0-9]+-[0-9]+-[0-9]+-0-1-0-.*\.htm$/i;
    if (url.match(topicMessagesRegex)) return 'topicmessages';
    return 'unknown';
}

async function entryPoint() {
    //let start = performance.now();
    const currentPageType = getCurrentPageType(`${window.location.pathname}${window.location.search}`);
    if (currentPageType === 'topicmessages') {
        const jvFluxCompagnon = new JVFluxCompagnon();
        await jvFluxCompagnon.init();
        await jvFluxCompagnon.handleTopicMessages();
    }
    //let end = performance.now();
    //console.log(`entryPoint total time = ${end - start} ms`);
}

entryPoint();
