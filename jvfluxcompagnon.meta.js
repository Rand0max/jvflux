// ==UserScript==
// @name        JVFlux Compagnon
// @namespace   jvflux
// @version     1.0.5
// @downloadURL https://github.com/Rand0max/jvflux/raw/master/jvfluxcompagnon.user.js
// @updateURL   https://github.com/Rand0max/jvflux/raw/master/jvfluxcompagnon.meta.js
// @author      Rand0max / JVFlux
// @description Intégration du wiki officiel JVFlux au sein des forums JVC
// @icon        https://jvflux.fr/skins/logo_wiki.png
// @match       http://www.jeuxvideo.com/forums/*
// @match       https://www.jeuxvideo.com/forums/*
// @connect     jvflux.fr
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addStyle
// @grant       GM_deleteValue
// @grant       GM_listValues
// @grant       GM_getResourceText
// @resource    JVFLUX_CSS https://raw.githubusercontent.com/Rand0max/jvflux/master/jvfluxcompagnon.css
// @require     jvfluxcompagnon.js
// @require     main.js
// @run-at      document-end
// ==/UserScript==