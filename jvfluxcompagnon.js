
class JVFluxCompagnon {

    constructor() {
        this.jvfluxUrl = 'https://jvflux.fr';
        this.jvfluxPageListUrl = 'https://archives.jvflux.fr/noreferer/pages.json';


        this.pageList = [];
        this.pageListRegex = new RegExp();

        this.storage_init = 'jvfluxcompagnon_init';
        this.storage_init_default = false;
        this.storage_pageList = 'jvfluxcompagnon_pageList';
        this.storage_pageList_default = [];
        this.storage_pageListLastUpdate = 'jvfluxcompagnon_pageListLastUpdate';
        this.storage_pageListLastUpdate_default = new Date(0);

        this.pageListRefreshExpire = 6;

        this.pageExclusions = ['rire', 'rires', 'jvc', 'pseudo', 'pseudos', 'musique', 'musiques', 'supprimer', 'topic', 'topics', 'forum', 'forums', 'forumeur', 'forumeurs', 'up', 'ahi', 'meme', 'même', 'mème', 'afk', 'aka', 'asap', 'btw', 'c/c', 'cad', 'càd', 'dl', 'dtc', 'fdp', 'ftg', 'ftw', 'gg', 'gl', 'hf', 'hs', 'ig', 'lel', 'lmao', 'lmfao', 'lol', 'maj', 'mdp', 'mdr', 'mmo', 'mmog', 'mmorpg', 'màj', 'nl', 'nsfw', 'omd', 'omfg', 'omg', 'over used', 'overused', 'pgm', 'pk', 'rofl', 'rpg', 'tg', 'vdm', 'wow', 'wtf', 'wth'];
    }

    async init() {
        await this.initStorage();
        const jvfluxCss = GM_getResourceText('JVFLUX_CSS');
        GM_addStyle(jvfluxCss);
    }

    escapeRegexPattern(str) {
        return str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }

    normalizeDiacritic(str) {
        return str.normalize("NFD").replace(/\p{Diacritic}/gu, '');
    }

    removeDoubleSpaces(str) {
        return str.replaceAll(/ +(?= )/g, '');
    }

    capitalize(str) {
        if (str.length === 0) return str;
        const regex = new RegExp(/\p{L}/, 'u');
        const i = str.search(regex);
        if (i < 0) return str;
        return str.substring(0, i) + str.charAt(i).toUpperCase() + str.slice(i + 1);
    }

    addArray(array1, array2) {
        array2.forEach(array1.add, array1);
        return array1;
    }

    jvfluxFullPreviewUrl = (page) => `${this.jvfluxUrl}/api.php?action=query&format=json&prop=info%7Cextracts%7Cpageimages%7Crevisions%7Cinfo&formatversion=2&redirects=true&exintro=true&exchars=525&explaintext=true&exsectionformat=plain&piprop=thumbnail&pithumbsize=480&pilicense=any&rvprop=timestamp&inprop=url&titles=${page}&smaxage=300&maxage=300&uselang=content&pithumbsize=600`;

    buildPageListRegex() {
        // \b ne fonctionne pas avec les caractères spéciaux
        const bStart = '(?<=\\W|^)';
        const bEnd = '(?=\\W|$)';
        let regexMap = this.pageList.map((e) => this.normalizeDiacritic(this.escapeRegexPattern(e)));
        this.pageListRegex = new RegExp(`${bStart}(${regexMap.join('|')})${bEnd}`, 'gi');
    }

    async queryPageList() {
        let newPageList = await fetch(this.jvfluxPageListUrl)
            .then(function (response) {
                if (!response.ok) throw Error(response.statusText);
                return response.text();
            })
            .then(function (text) {
                return JSON.parse(text);
            })
            .catch(function (err) {
                console.warn(err);
                return undefined;
            });

        if (!newPageList) return;

        newPageList = newPageList.filter((page) => !this.pageExclusions.includes(page.toLowerCase()));

        this.pageList = [...new Set(newPageList)];

        GM_setValue(this.storage_pageList, JSON.stringify(this.pageList));
        GM_setValue(this.storage_pageListLastUpdate, Date.now());
    }

    mustRefreshPageList() {
        let pageListLastUpdate = new Date(GM_getValue(this.storage_pageListLastUpdate, this.storage_pageListLastUpdate_default));
        let datenow = new Date();
        let dateOneDayOld = new Date(datenow.setHours(datenow.getHours() - this.pageListRefreshExpire));
        return pageListLastUpdate <= dateOneDayOld;
    }

    async loadStorage() {
        this.pageList = JSON.parse(GM_getValue(this.storage_pageList, this.storage_pageList_default));
        if (!this.pageList?.length || this.mustRefreshPageList()) await this.queryPageList();
        this.buildPageListRegex();
    }

    async initStorage() {
        let isInit = GM_getValue(this.storage_init, this.storage_init_default);
        if (isInit) {
            await this.loadStorage();
            return false;
        }
        else {
            await this.queryPageList();
            this.buildPageListRegex();
            GM_setValue(this.storage_init, true);
            return true;
        }
    }

    findArticleTitle(match) {
        let articleTitle = this.pageList.find(p => p.toLowerCase() === match.toLowerCase());
        if (articleTitle?.length > 0) return articleTitle;
        return this.pageList.find(p => this.normalizeDiacritic(p.toLowerCase()) === this.normalizeDiacritic(match.toLowerCase()));
    }

    buildArticleLink(match, className = 'jvflux-link') {
        const articleTitle = this.findArticleTitle(match);
        const url = `${this.jvfluxUrl}/${articleTitle.replaceAll(' ', '_')}`;
        return `<a href="${url}" target="_blank" class="xXx ${className}" pagetitle="${articleTitle}" title="Consulter la page &quot;${articleTitle}&quot; dans JVFlux">${match}</a>`;
    }

    formatMatches(matches) {
        let formatMatch = (str) => this.capitalize(this.removeDoubleSpaces(str.trim()));
        let matchesSorted = [...matches].map(formatMatch).sort();
        let matchesHtml = '';
        let index = 0;
        matchesSorted.forEach((match) => {
            const className = `jvflux-match${index < matchesSorted.length - 1 ? ' match-after' : ''}`;
            matchesHtml += this.buildArticleLink(match, className);
            index++;
        });
        return matchesHtml;
    }

    addRightBlocMatches(matches) {
        if (matches?.size === 0) return;

        let html = '';
        html += '<div class="card card-jv-forum card-forum-margin">';
        html += `<div class="card-header">JVFLUX</div>`;
        html += '<div class="card-body">';
        html += '<div class="scrollable">';
        html += '<div class="scrollable-wrapper">';
        html += '<div id="jvflux-matches-content" class="scrollable-content bloc-info-forum">';


        html += `<div id="jvflux-matches-wrapper">`;
        html += `<div id="jvflux-matched">${this.formatMatches(matches)}</div>`;
        html += '</div>';

        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';

        let matchesBloc = document.createElement('div');
        document.querySelector('#forum-right-col').append(matchesBloc);
        matchesBloc.outerHTML = html;
    }

    getAllMessages(doc) {
        let allMessages = doc.querySelectorAll('.conteneur-messages-pagi > div.bloc-message-forum');
        return [...allMessages];
    }

    highlightTextMatches(element, matches) {
        let content = element.textContent;

        // reversed to simplify algo because we set 'content' at each iteration and move the cursor
        matches.reverse().every(match => {
            const normMatch = match[0];
            if (match.index <= -1) return false;
            const realMatchContent = content.slice(match.index, match.index + normMatch.length);
            const newMatchContent = this.buildArticleLink(realMatchContent);
            content = `${content.slice(0, match.index)}${newMatchContent}${content.slice(match.index + normMatch.length, content.length)}`;
            return true;
        });

        if (element.nodeType == Node.TEXT_NODE) {
            let newNode = document.createElement('a');
            element.parentElement.insertBefore(newNode, element);
            element.remove();
            newNode.outerHTML = content;
        }
        else {
            element.innerHTML = content;
        }
    }

    handleMatches(textChild, highlightedMatches) {
        const normContent = this.normalizeDiacritic(textChild.textContent);
        const matches = [...normContent.matchAll(this.pageListRegex)];

        const filteredMatches = matches.filter(m => !highlightedMatches.has(m[0]));
        if (filteredMatches.length === 0) return undefined;

        this.highlightTextMatches(textChild, filteredMatches);

        return filteredMatches.map(m => m[0]);
    }

    handleMessageChildren(element, highlightedMatches) {
        const allowedTags = ['P', 'STRONG', 'U', 'I', 'EM', 'B'];
        const getParagraphChildren = (elem) => [...elem.children].filter(c => allowedTags.includes(c.tagName) && c.textContent.trim() !== '');
        const getTextChildren = (elem) => [...elem.childNodes].filter(c => c.nodeType === Node.TEXT_NODE && c.textContent.trim() !== '');

        // Un message contient une balise p pour chaque ligne
        // Un p peut contenir du texte ou du html (img, a, etc ET strong, b, u, etc)

        let paragraphChildren = getParagraphChildren(element);
        paragraphChildren.forEach(paragraph => {
            const textChildren = getTextChildren(paragraph); // on ne s'intéresse qu'au texte
            textChildren.forEach(textChild => {
                // on traite le texte du noeud courant
                let matchesFound = this.handleMatches(textChild, highlightedMatches);
                if (matchesFound?.length) {
                    highlightedMatches = this.addArray(highlightedMatches, matchesFound);
                }
            });
            // puis on traite ses enfants

            let subChildRes = this.handleMessageChildren(paragraph, highlightedMatches);
            if (subChildRes?.length) {
                highlightedMatches = this.addArray(highlightedMatches, subChildRes);
            }
        });
        return highlightedMatches;
    }

    handleMessage(message) {
        let contentElement = message.querySelector('.txt-msg.text-enrichi-forum');
        if (!contentElement) return;
        return this.handleMessageChildren(contentElement, new Set());
    }

    async previewArticleCallback(pagetitle) {
        const url = this.jvfluxFullPreviewUrl(pagetitle);

        let previewContent = await fetch(url)
            .then(function (response) {
                if (!response.ok) throw Error(response.statusText);
                return response.text();
            })
            .then(function (res) {
                return JSON.parse(res);
            })
            .catch(function (err) {
                console.warn(err);
                return undefined;
            });

        if (!previewContent) return undefined;

        const resPages = previewContent.query.pages;
        const rootPage = resPages[Object.keys(resPages)[0]];
        const extract = rootPage.extract?.trim();
        const thumbnail = rootPage.thumbnail?.source;

        return { extract: extract, thumbnail: thumbnail };
    }

    async onPreviewHover(element) {
        if (!element.hasAttribute('pagetitle') || element.children.length > 0) return;
        const previewContent = await this.previewArticleCallback(element.getAttribute('pagetitle'));

        if (!previewContent?.extract) return;

        const mwPopupThumbnail = previewContent.thumbnail?.length > 0 ?
            `<a class="mwe-popups-discreet" href="${element.getAttribute('href')}"><img class="mwe-popups-thumbnail" src="${previewContent.thumbnail}"/></a>` : '';

        const mwPopupHtml =
            `<div class="mwe-popups mwe-popups-type-page mwe-popups-fade-in-up mwe-popups-no-image-pointer mwe-popups-is-tall" aria-hidden="">
        <div class="mwe-popups-container">
        ${mwPopupThumbnail}
        <a class="mwe-popups-extract">${previewContent.extract}</a>
        </div>
        </div>`;

        let popupContainer = document.createElement('div');
        element.appendChild(popupContainer);
        popupContainer.outerHTML = mwPopupHtml;
    }

    async handleTopicMessages() {
        const allMessages = this.getAllMessages(document);
        let allMatches = new Set();
        allMessages.forEach((message) => {
            const messageMatches = this.handleMessage(message);
            if (messageMatches?.length) messageMatches.forEach((m) => allMatches.add(m.trim().toLowerCase()));
        });
        const allLinks = document.querySelectorAll('.jvflux-link');
        allLinks.forEach(link => {
            link.onclick = () => this.onPreviewHover(link);
            link.onmouseover = () => this.onPreviewHover(link);
            link.ontouchstart = () => this.onPreviewHover(link);
        });
        this.addRightBlocMatches(allMatches);
    }

}
